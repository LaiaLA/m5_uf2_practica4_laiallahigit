
import java.util.Scanner;

/**
 * <h2>classe LaCalculadora, Utilitzada per a fer càlculs numerics bàsics </h2>
 * 
 * @version 5-2021
 * @author Laia Llahí
 * @since 6-5-2021
 */
public class LaCalculadora {
//comentario creado desde la web de GitLab
	/**
	 * @param reader Scanner per llegir el que entra per consola l'usuari
	 */
    static Scanner reader = new Scanner(System.in);
//Afegim un altre comentari
    /**
     * S'encarrega de recollir totes les crides a les funcions per a que la calculadora sigui possible.
     * @param op1 És on es troba el primer valor numeric
     * @param op2 És on es troba el segon valor numeric
     * @param opcio És on es troba el caracter que determina quin calcul es vols fer.
     * @param resultat És on es troba el valor numeric final
     * @param control Marca si han entrat els dos valors numerics per pantalla.
     * 
     */
	public static void main(String[] args) {
		
		int op1 = 0, op2 = 0;
		char opcio;
		int resultat = 0;
		boolean control = false;

		do {
			mostrar_menu();
			opcio = llegirCar();

			switch (opcio) {
                case 'o':
                case 'O':
                    op1 = llegirEnter();
                    op2 = llegirEnter();
                    control = true;
                    break;
                case '+':
                    if (control)
                        resultat = suma(op1, op2);
                    else mostrarError();
                    break;
                case '-':
                    if (control)
                        resultat = resta(op1, op2);
                     else mostrarError();
                    break;
                case '*':
                    if (control)
                        resultat = multiplicacio(op1, op2);
                     else mostrarError();
                    break;
                case '/':
                    if (control) {
                        resultat = divisio(op1, op2);
                        if (resultat == -99)
                            System.out.print("No ha fet la divisiÃ³");
                        else
                             visualitzar(resultat);
					}
		            else mostrarError();
                    break;
                case 'v':
                case 'V':
                    if (control)
                        visualitzar(resultat);
                     else mostrarError();
                    break;
                case 's':
                case 'S':
                    System.out.print("Acabem.....");

                    break;
                default:
                    System.out.print("opciÃ³ erronia");
			}
			
		} while (opcio != 's' && opcio != 'S');
		System.out.print("\nAdeu!!!");

	}

	/**
	 * Mostra per pantalla el text d'error 
	 * 
	 */
    public static void mostrarError() { /* procediment */
		System.out.println("\nError, cal introduir primer els valors a operar");
	}



    /**
	 * S'encarrega de fer l'operació de sumar
	 * @param res És on es guarda el resultat de la suma
	 * @param a És on es troba el primer valor numeric
	 * @param b És on es troba el segon valor numeric
	 * @see main
	 * @return res Retorna el resultat de l'operació suma
	 */
	public static int suma(int a, int b) { /* funciÃ³ */
		int res;
		res = a + b;
		return res;
	}

	/**
	 * S'encarrega de fer l'operació de restar
	 * @param res És on es guarda el resultat de la resta
	 * @param a És on es troba el primer valor numeric
	 * @param b És on es troba el segon valor numeric
	 * @see main
	 * @return res Retorna el resultat de l'operació resta
	 */
	public static int resta(int a, int b) { /* funciÃ³ */
		int res;
		res = a - b;
		return res;
	}

	/**
	 * S'encarrega de fer l'operació de multiplicar
	 * @param res És on es guarda el resultat de la multiplicacio
	 * @param a És on es troba el primer valor numeric
	 * @param b És on es troba el segon valor numeric
	 * @see main
	 * @return res Retorna el resultat de l'operació multiplicar
	 */
	public static int multiplicacio(int a, int b) { /* funciÃ³ */
		int res;
		res = a * b;
		return res;
	}

	/**
	 * S'encarrega de fer l'operació de dividir
	 * @param res És on es guarda el resultat de la divisio
	 * @param op És on es guarda el caràcter introduit per consola.
	 * @param a És on es troba el primer valor numeric
	 * @param b És on es troba el segon valor numeric
	 * @see main
	 * @return res Retorna el resultat de l'operació dividir
	 */
	public static int divisio(int a, int b) { /* funciÃ³ */
		int res = -99;
		char op;

		do {
			System.out.println("M. " + a + " mod " + b);
			System.out.println("D  " + a + " / " + b);
			op = llegirCar();
			if (op == 'M' || op == 'm'){
                if (b == 0) 
                    System.out.print("No es pot dividir entre 0\n");
                else
                    res = a % b;
			}

			else if (op == 'D' || op == 'd'){
                    if (b == 0)
                        System.out.print("No es pot dividir entre 0\n");
                    else
                        res = a / b;
                    }
                else
                    System.out.print("opciÃ³ incorrecte\n");
		} while (op != 'M' && op != 'm' && op != 'D' && op != 'd');

		return res;
	}

	/**
	 * S'encarrega de llegir el caracter introduit per pantalla
	 * @param car És on es guarda el caràcter introduit per consola.
	 * @return car Retorna el caracter
	 */
	public static char llegirCar() // funciÃ³
	{
		char car;

		System.out.print("Introdueix un carÃ¡cter: ");
		car = reader.next().charAt(0);

		//reader.nextLine();
		return car;
	}

	/**
	 * S'encarrega de llegir els valors numerics introduits per pantalla
	 * @param valor És on es guarda el valor numeric introduit per consola.
	 * @param valir És un boleà amb el que mirarem si el valor introduit es enter o no.
	 * @return valor Retorna el valor numeric
	 */
	public static int llegirEnter() // funciÃ³
	{
		int valor = 0;
		boolean valid = false;

		do {
			try {
				System.out.print("Introdueix un valor enter: ");
				valor = reader.nextInt();
				valid = true;
			} catch (Exception e) {
				System.out.print("Error, s'espera un valor enter");
				reader.nextLine();
			}
		} while (!valid);

		return valor;
	}

	/**
	 * Mostra per pantalla el text del resultat
	 * @param res És el resultat de l'operació
	 * @see main 
	 * 
	 */
	public static void visualitzar(int res) { /* procediment */
		System.out.println("\nEl resultat de l'operacio Ã©s " + res);
	}

	/**
	 * Mostra per pantalla el text que conté les opcions. 
	 * 
	 */
	public static void mostrar_menu() {
		System.out.println("\nCalculadora:\n");
		System.out.println("o.- Obtenir els valors");
		System.out.println("+.- Sumar");
		System.out.println("-.- Restar");
		System.out.println("*.- Multiplicar");
		System.out.println("/.- Dividir");
		System.out.println("v.- Visualitzar resultat");
		System.out.println("s.- Sortir");
		System.out.print("\n\nTria una opciÃ³ i ");
	}
	
	/**
	 * S'encarrega de fer l'operació de dividir
	 * @param res És on es guarda el resultat de la divisio
	 * @param num1 És on es troba el primer valor numeric
	 * @param num2 És on es troba el segon valor numeric
	 * @return res Retorna el resultat de l'operació dividir
	 */
	public static  int divideix(int num1, int num2) {
		int res;
		if (num2 == 0)
			throw new java.lang.ArithmeticException("Divisió entre zero");
		else 
		res = num1/num2;
			return res;
	}
	
	

}
