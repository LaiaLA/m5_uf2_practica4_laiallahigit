import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
class  LaCalculadoraTest {

	@Test
	public void testSuma() {
		int res = LaCalculadora.suma(10, 20);
		assertEquals(30,  res);
	//	fail("Not yet implemented");
	}

	@Test
	void testResta() {
		int res = LaCalculadora.resta(10, 20);
		//assertEquals(30,  res);
		assertEquals(52,res,"falla");
	//	fail("Not yet implemented");
	}

	@Test
	void testMultiplicacio() {
		int res = LaCalculadora.multiplicacio(10,9);
		assertEquals(30,  res);
	//	fail("Not yet implemented");
	}

	@Test
	void testDivisio() {
		int res = LaCalculadora.divisio(10,2 );
		assertEquals(30,  res);
	//	fail("Not yet implemented");
	}
	
	@Test
	void testDivideix() {
		int res = LaCalculadora.divideix(12,  0);
		assertEquals(30,  res);
	}
	
	@Test
	public void testException() {
		int res;
		try {
			res = LaCalculadora.divideix(10, 0);
			fail("Fallo: Passa per aqu� si no es llan�a l�excepci� ArithmeticException");
		}
		catch (ArithmeticException e) {
			System.out.print("La prova funciona correctament\n");
			//La prova funciona correctament
		}
    }
}
