import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ CalculadoraTestDivi.class, CalculadoraTestMulti.class, CalculadoraTestResta.class,
		CalculadoraTestSuma.class, LaCalculadoraTest.class })
public class AllTests {

}
